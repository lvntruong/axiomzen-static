(function() {
  'use strict';

  $(document).ready(function() {

    $('.js-apply-now').click(function(e) {
      e.preventDefault();
      $('html, body').animate({
        scrollTop: $('.js-section-positions').offset().top - 60
      }, 1000);
    });

    $('.js-learn-more').click(function(e) {
      e.preventDefault();
      $('html, body').animate({
        scrollTop: $('.js-section-process').offset().top - 60
      }, 1000);
    });

    $('.js-tab-careers').click(function(e) {
      e.preventDefault();
      var $this = $(this);
      var $tabContent = $('.js-content-' + $this.attr('id'));
      var $container = $this.closest('.js-container');
      $this.addClass('is-active');
      $this.siblings().removeClass('is-active');
      $tabContent.animate({opacity: 1}, 400).addClass('is-active');
      $container.children('.js-tab-content-careers')
        .not($tabContent)
        .animate({opacity: 0}, 400)
        .removeClass('is-active');
    });

    $('.js-department-header').click(function() {
      var $this = $(this);
      var index = $this.data('index');
      var $category = $('.js-positions.js-category-' + index);
      var $arrow = $('.js-arrow.js-category-' + index);
      $category.slideToggle(300);
      $arrow.toggleClass('is-rotated');
    });

    $('.js-interview-expand').click(function() {
      if ($('.js-interview-list').hasClass('is-open')) {
        $('.js-interview-list').removeClass('is-open');
        $(this).text('Show All');
      } else {
        $('.js-interview-list').addClass('is-open');
        $(this).text('Hide');
      }
    });
  });
})();
