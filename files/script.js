$(function() {
  'use strict';

  var $tabsBtn = $('.js-tabsBtn');
  var $tabsSwitcher = $('.js-tabsSwitcher');

  $(document).ready(function() {
    jumpToAnchor();
  });

  window.addEventListener('hashchange', jumpToAnchor);

  function jumpToAnchor() {
    var urlHash = window.location.hash.substring(1);

    if (!urlHash || !$('#' + urlHash).length) {
      return;
    }
    var $tabsBtn = $('#' + urlHash);
    var $tabContent = $('.js-content-' + urlHash);
    var switcherPosition = $tabsBtn.index() * 100;

    $('html, body').animate({
      scrollTop: $tabsBtn.offset().top - 100
    }, 0);

    $tabsSwitcher.css('transform', 'translateX('+ switcherPosition +'%)');
    $tabsBtn.addClass('is-switched').siblings().removeClass('is-switched');
    $tabContent.addClass('is-active').animate({opacity: 1, height: '100%'}, 400);
    $tabContent.siblings()
      .removeClass('is-active')
      .animate({opacity: 0, height: '0%'}, 400);
   }

  /**
   * Font observations
   */

  var fontFaceObserver = new window.FontFaceObserver('CalibreWeb');

  fontFaceObserver.check(null, 5000)
    .then(function() {
      $('html').addClass('fonts-loaded');
    })
    .catch(function() {
      console.log('CalibreWeb failed to load.');
    });

  /**
  * Nav menu set active page
  */

  var currentPage = location.pathname.split('/')[1];

  // Don't run on homepage
  if(currentPage !== '') {
    $('.js-nav-menuLink[href^="/' + currentPage + '"]').addClass('is-active');
    $('.js-nav-menuLink:not(.is-active)').addClass('is-inactive');
  }

  /**
   * Nav menu toggling
   */

  var $navDrawer = $('.js-nav-drawer');
  var $navDrawerToggle = $('.js-nav-drawerToggle');
  var $navDrawerLink = $('.js-nav-drawerLink');

  $navDrawerToggle.click(function (e) {
    e.preventDefault();
    $navDrawer.fadeToggle(400);
    $navDrawerLink.toggleClass('is-open');
    $navDrawerToggle.text(function(i, text){return text === 'Menu' ? 'Close' : 'Menu';});
  });

  $navDrawerLink.click(function () {
    $navDrawer.fadeToggle(400);
    $navDrawerLink.removeClass('is-open');
  });

  /**
   * Tabs toggling
   */

  $tabsBtn.click(function (e) {
    e.preventDefault();
    var hash = $(this).attr('id');
    var switcherPosition = $(this).index() * 100;
    var $tabContent = $('.js-content-' + hash);
    window.location.hash = hash;
    $tabsSwitcher.css('transform', 'translateX('+ switcherPosition +'%)');
    $(this).addClass('is-switched').siblings().removeClass('is-switched');
    $tabContent.addClass('is-active').animate({opacity: 1, height: '100%'}, 400);
    $tabContent.siblings()
      .removeClass('is-active')
      .animate({opacity: 0, height: '0%'}, 400);
  });

  /**
   * Warning toggle
   */

  var $warning = $('.js-warning');
  var $warningBtn = $('.js-warningBtn');

  $warningBtn.click(function (e) {
    e.preventDefault();
    $warning.toggleClass('is-open');
  });

  /**
   * Newsletter subscriptions
   */

  var $newsletterForm = $('.js-newsletter');
  var $newsletterText = $newsletterForm.find('.js-newsletter-input');
  var $newsletterSubmit = $newsletterForm.find('.js-newsletter-btn');
  var $newsletterLog = $newsletterForm.find('.js-newsletter-log');
  var $newsletterLoader = $newsletterForm.find('.js-newsletter-loader');

  $newsletterSubmit.on('click', function(e) {
    var emailVal = $newsletterText.val();

    // If there is no email address, do nothing
    if (emailVal.indexOf('@') < 0 || emailVal.length < 5) {
      return false;
    }

    e.preventDefault();
    submitHandler();
  });

  /**
   * Scroll reveal animations
   */

  var sr = new ScrollReveal();

  sr.reveal('.js-reveal', {
    distance : '6rem',
    scale: 1
  }, 100);

  sr.reveal('.js-revealHero', {
    distance : '3rem',
    scale: 1,
    delay: 200
  });

  sr.reveal('.js-revealGallery', {
    distance : '6rem',
    scale: 1,
    reset: true
  }, 100);

  // Submit handler, sends ajax req to server
  function submitHandler() {
    $.ajax({
      url: '/signup',
      method: 'POST',
      dataType: 'json',
      data: {
        email: $newsletterText.val()
      },
      beforeSend: function() {
        $newsletterLog.removeClass('is-active');
        $newsletterLoader.addClass('is-active');
      },
      error: function(req) {
        var err = JSON.parse(req.responseText);
        $newsletterLog.html(err.message);
        $newsletterLog.addClass('is-active');
        $newsletterLoader.removeClass('is-active');
      },
      success: function(data, status) {
        $newsletterLog.html(data.message);
        $newsletterLog.addClass('is-active');
        $newsletterLoader.removeClass('is-active');
        $newsletterText.val('');
      }
    });
  }

  /**
   * Slick carousel
   */
    var slickOptions = {
    autoplay: false,
    arrows: false,
    dots: true,
    infinite: false,
    centerMode: true,
    centerPadding: '0px',
    mobileFirst: true,
    variableWidth: true,
    adaptiveHeight: false,
    initialSlide: 1
  };

  $('.js-slick').slick(slickOptions);
});
