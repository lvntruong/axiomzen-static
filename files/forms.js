$(function() {
  'use strict';

  /**
   * Forms
   */
  var $form = document.querySelector('.js-form') || '';
  var $section = document.querySelector('.js-form-section') || '';
  var $formElementErrors = $form ? $form.querySelectorAll('.js-form-error') : '';
  var $submit = $('.js-form-submit');
  var $successLog = $('.js-msg-success');
  var $errorLog = $('.js-msg-error');
  var $formLoader = $('.js-form-loader');
  var $formInput = $('.js-form-input');
  var $radio = $('input[type=radio][name=optionHelp]');
  var $checkbox = $('input[type=checkbox]');
  var $formWrapper = $('.js-form-wrapper');
  var $openPositionsLink = $('.js-form-positions');

  $formInput.on('blur', function() {
    if (!validateInput(this, $formElementErrors)) {
     return;
    }
  });

  $radio.on('change', function() {
    var radioName = $(this).prop('id');

    var collapseBlocks = function() {
      $checkbox.prop('checked', false);
      $('.js-block-project, .js-block-strategy, .js-block-design, .js-block-engineering').addClass('is-collapsed');
    };

    if (radioName === 'project') {
      $('.js-block-project').removeClass('is-collapsed');
      $openPositionsLink.removeClass('is-active--inlineBlock');
    } else if (radioName === 'team') {
      collapseBlocks();
      $openPositionsLink.addClass('is-active--inlineBlock');
    } else if (radioName === 'investor') {
      collapseBlocks();
      $openPositionsLink.removeClass('is-active--inlineBlock');
    }
  });

  $checkbox.on('change', function() {
    var checkboxName = $(this).prop('id');

    if (this.checked) {
      $('.js-block-' + checkboxName).removeClass('is-collapsed');
    } else {
      $('.js-block-' + checkboxName).addClass('is-collapsed');
    }
  });

  $submit.on('click', function(e) {
    e.preventDefault();
    formOnSubmit($form, $formElementErrors);
  });

  function formOnSubmit($form, $formElementErrors) {
    var valid = true;

    $.each($form.elements, function(key, formElement) {
      if (!validateInput(formElement, $formElementErrors)) {
        valid = false;
      }
    });

    if (!valid) {
      $section.scrollIntoView();
      return;
    }
    var body = {};
    $.each($form.elements, function(key, element) {

      element.name = $(element).prop('name');
      element.id = $(element).prop('id');

      switch (element.type) {
        case 'submit':
        break;
        case 'hidden':
        break;
        case 'radio':
        if (element.checked) {
          body[element.name] = {
            name: element.name,
            id: element.id,
            value: element.checked ? element.value : '',
            type: element.type,
            required: element.required
          };
        }
        break;
        case 'checkbox':
          var services = [];
          var servicesSalesForce = [];
          var typeOfProject = [];

          if (!$(element).hasClass('js-type-of-project')) {
            break;
          }
          $('.js-block-' + element.id + ' input.js-checklist-option:checked')
            .each(function() {
            var item = $(this).closest('.js-input-wrapper')
              .find('.js-checklist-item p').text();
            var salesforceValue = $(this).data('salesforce');
            services.push(item);
            servicesSalesForce.push({raw: salesforceValue});
          });

          $('.js-block-project input.js-type-of-project:checked').each(function() {
            var index = $(this).data('salesforce');
            typeOfProject.push({raw: index});
            body['typeOfProject'] = {
              name: 'typeOfProject',
              value: typeOfProject,
              type: 'checkgroup',
              required: element.required
            };
          });

          if (!$(element).data('subType')) {
            body[element.name] = {
              name: element.name,
              value: element.checked ? 'YES' : 'NO',
              type: element.type,
              required: element.required,
              services: services,
              servicesSalesForce: servicesSalesForce
            };
          }
        break;
        default:
          body[element.name] = {
            name: element.name,
            value: element.value,
            type: element.type,
            required: element.required
          };
       }
    });
    submitHandler(body);
  }

  function validateInput(formElem, errorTags) {
    var errorTag;
    formElem.value = validator.trim(formElem.value);

    $.each(errorTags, function(key, value) {
      if ($(value).data('error-for') === $(formElem).prop('id')) {
        errorTag =  value;
      }
    });

    switch (formElem.type) {
      case 'submit':
      break;
      case 'hidden':
      break;
      case 'select-one':
        if (formElem.required && validator.isNull(formElem.value) && errorTag) {
          $(errorTag).text('Required').addClass('is-active');
          return false;
        } else if(formElem.required && !validator.isNull(formElem.value) && errorTag) {
          $(errorTag).text('').removeClass('is-active');
          $(formElem).addClass('is-filled');
          return true;
        }
      break;
      case 'email':
        if (formElem.required && validator.isNull(formElem.value) && errorTag) {
          $(errorTag).text('Required').addClass('is-active');
          return false;
        } else if (formElem.required && !validator.isEmail(formElem.value) && errorTag) {
          $(errorTag).text('Invalid email').addClass('is-active');
          $(formElem).addClass('is-filled');
          return false;
        } else if (formElem.required && validator.isEmail(formElem.value) && errorTag) {
          $(errorTag).text('').removeClass('is-active');
          $(formElem).addClass('is-filled');
          return true;
        }
      break;
      default:
        if (formElem.required && validator.isNull(formElem.value) && errorTag) {
          $(errorTag).text('Required').addClass('is-active');
          return false;
        } else if (formElem.required && !validator.isNull(formElem.value) && errorTag) {
          $(errorTag).text('').removeClass('is-active');
          $(formElem).addClass('is-filled');
          return true;
        }
    }
    return true;
  }

  function trackSuccess(eventMessage) {

    // Track Mixpanel
    mixpanel.track('Page Viewed', {
      'Event' : eventMessage,
      'Page Name': document.title,
      'URL': window.location.pathname
    });

    // Track Google Analytic Event Goal
    ga('send', {
      'hitType': 'event',
      'eventCategory': 'Form',
      'eventAction': 'submit',
      'eventLabel': 'Contact Success'
    });
  }

  function submitHandler(body) {
    $.ajax({
    type: 'POST',
    url: $form.action,
    contentType: 'application/json',
    data: JSON.stringify(body),
    beforeSend: function() {
      $formLoader.addClass('is-active');
      $submit.attr('disabled', true).text('Sending...');
      $errorLog.removeClass('is-active');
    }})
    .done(function(data) {
      trackSuccess(data);
      $section.scrollIntoView();
      $formLoader.removeClass('is-active');
      $formWrapper.addClass('is-hidden');
      $successLog.addClass('is-active');
    })
    .fail(function(error) {
      $section.scrollIntoView();
      $submit.attr('disabled', false).text('Submit');
      $formLoader.removeClass('is-active');
      $errorLog.addClass('is-active');
      $errorLog.find('p.u-text-error')
        .html(error.responseText +
          '<br> Please try again or contact us at hello@axiomzen.co');
    });
  }
});
